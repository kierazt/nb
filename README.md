# README #

Cara setting web di APACHE Windows

1. setting virtual host apache terlebih dahulu,

	-> buka file explorer 
	
	-> masuk kedalam drive instalasi xampp/wampp/apache 
	
	-> masuk kedalam C:/Xampp/apache/conf/extra (asumsi windows dan apache server di dalam xampp)
	
	-> edit file httpd-vhosts.conf
	
	-> hapus karakter #
	
		- pada : NameVirtualHost *:80
		
		- dan pada :  
		
			<VirtualHost *:80>
				ServerAdmin admin@admin.com
				DocumentRoot "C:\xampp\htdocs\nb\web"
				ServerName nature-beauty.local
				ServerAlias www.nature-beauty.local
				ErrorLog "logs/nature-beauty.local-error.log"
				CustomLog "logs/nature-beauty.local-access.log" common
			</VirtualHost>
			
	-> pastikan documen root menunjuk pada direktori web, karena web index berada disini.
	
	-> save dan exit
	
	-> kemudian buka notepad as administrator
	
	-> kemudian buka file ke C:/system32/drivers/etc/hosts
	
	-> lalu edit isi nya :
	
		- hapus karakter # menjadi 127.0.0.1	localhost
		
		- dan tambahkan di belakang localhost [spasi] nama_virtual_host (contoh : nature-beauty.local)
		
		- save dan exit.
	
	
			
2. dump atau import db di mysql
	-> buka localhost/phpmyadmin
	-> buat db baru (samakan nama db dengan yang ada di direktory db di web source)
	-> setelah proses import selesai restart apache dan mysql
	
3. kemudian akses domain nature-beauty.local untuk mengecek apakah domain telah dapat diakses


Cara pull/tarik source web di git (bitbucket) dengan git-scm

1. buka htdocs apache (C:/Xampp/htdocs/)
2. klik kanan pada layar dan pilih open git bash here
3. kemudian copas kode berikut " git clone https://kierazt@bitbucket.org/kierazt/nb.git " (copy diantara tanda ")
4. lalu tekan enter.
5. maka secara otomatis source code akan di clone atau terunduh di direktori htdocs dengan direktory baru.
