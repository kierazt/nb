-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2019 at 03:21 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_nature_beauty`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ajax_funcs`
--

CREATE TABLE `tbl_ajax_funcs` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ajax_funcs`
--

INSERT INTO `tbl_ajax_funcs` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'global_uri', '\'var global_uri = function(link){\r\n                return \"\' . global_uri($this->config->module_name) . \'\"+link\r\n            };\'', '-', 1, 1, '2019-01-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_categories`
--

CREATE TABLE `tbl_cms_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_category_contents`
--

CREATE TABLE `tbl_cms_category_contents` (
  `id` int(32) NOT NULL,
  `content_id` int(32) NOT NULL,
  `content_category_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_comments`
--

CREATE TABLE `tbl_cms_comments` (
  `id` int(32) NOT NULL,
  `text` text NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_block` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_contents`
--

CREATE TABLE `tbl_cms_contents` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  `is_footer` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '0',
  `is_page` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` int(32) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_content_photos`
--

CREATE TABLE `tbl_cms_content_photos` (
  `id` int(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_messages`
--

CREATE TABLE `tbl_component_messages` (
  `id` int(32) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from_id` int(32) NOT NULL,
  `to_id` int(32) NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `label_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_categories`
--

CREATE TABLE `tbl_component_message_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_labels`
--

CREATE TABLE `tbl_component_message_labels` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_message_status`
--

CREATE TABLE `tbl_component_message_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_message_status`
--

INSERT INTO `tbl_component_message_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'unread', '-', 1, 1, '2019-02-11 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-11 00:00:00'),
(3, 'archive', '-', 1, 1, '2019-02-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notifications`
--

CREATE TABLE `tbl_component_notifications` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `status_id` int(32) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notification_categories`
--

CREATE TABLE `tbl_component_notification_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_notification_categories`
--

INSERT INTO `tbl_component_notification_categories` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'network', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'server', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'system', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'database', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_notification_status`
--

CREATE TABLE `tbl_component_notification_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_component_notification_status`
--

INSERT INTO `tbl_component_notification_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'pending', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'replied', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'archive', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_tasks`
--

CREATE TABLE `tbl_component_tasks` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `progress` int(3) NOT NULL,
  `description` text NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_task_categories`
--

CREATE TABLE `tbl_component_task_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_task_status`
--

CREATE TABLE `tbl_component_task_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_configs`
--

CREATE TABLE `tbl_configs` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `is_static` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_configs`
--

INSERT INTO `tbl_configs` (`id`, `keyword`, `value`, `is_static`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'app_name', 'nature-beauty', 0, 1, 1, '2018-07-23 00:00:00'),
(2, 'lang_id', '28173tgfds7tf', 0, 1, 1, '2018-07-23 00:00:00'),
(3, 'lang_name', 'english', 0, 1, 1, '2018-07-23 00:00:00'),
(4, 'salt', '834red567gh4765vbfr76538', 0, 1, 1, '2018-07-23 00:00:00'),
(5, 'session_name', 'd98786tayghdjaw90d87atw', 0, 1, 1, '2018-07-23 00:00:00'),
(6, 'website_id', 'd9s8a7yudhioas987dyuhss', 0, 1, 1, '2018-07-23 00:00:00'),
(7, 'cookie_id', '90daw786tyghdjioaw987d6', 0, 1, 1, '2018-07-23 00:00:00'),
(8, 'redirect_success_login_backend', 'backend/dashboard', 0, 1, 1, '2018-07-23 00:00:00'),
(9, 'redirect_failed_login_backend', 'backend/login', 0, 1, 1, '2018-07-23 00:00:00'),
(10, 'mod_active', 'frontend', 0, 1, 1, '2018-11-04 00:00:00'),
(11, 'controller_active', 'home', 0, 1, 1, '2018-11-04 00:00:00'),
(12, 'global_title_en', 'welcome to nature beauty', 0, 1, 1, '2018-11-09 07:53:48'),
(13, 'layout_frontend', 'metronic', 0, 1, 1, '2018-11-16 20:32:48'),
(14, 'uri_img_item_color', 'media/img/items/colors/', 0, 1, 1, '2019-01-03 09:45:11'),
(15, 'uri_img_item_brand', 'media/img/items/brands/', 0, 1, 1, '2019-01-03 09:45:41'),
(16, 'dev_status', '1', 0, 1, 1, '2019-02-01 00:00:00'),
(17, 'footer_about', ' <aside class=\"f_widget ab_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>About Me</h3>\r\n                    </div>\r\n                    <p>If you own an Iphone, you’ve probably already worked out how much fun it is to use it to watch movies-it has that nice big screen, and the sound quality.</p>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(18, 'footer_newsletter', '<aside class=\"f_widget news_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Newsletter</h3>\r\n                    </div>\r\n                    <p>Stay updated with our latest trends</p>\r\n                    <div id=\"mc_embed_signup\">\r\n                        <form target=\"_blank\" method=\"post\" class=\"subscribes\">\r\n                            <div class=\"input-group d-flex flex-row\">\r\n                                <input name=\"EMAIL\" placeholder=\"Enter email address\" onfocus=\"this.placeholder = \'\'\" onblur=\"this.placeholder = \'Email Address \'\" required=\"\" type=\"email\">\r\n                                <button class=\"btn sub-btn\"><span class=\"lnr lnr-arrow-right\"></span></button>		\r\n                            </div>				\r\n                            <div class=\"mt-10 info\"></div>\r\n                        </form>\r\n                    </div>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(19, 'footer_socials', '<aside class=\"f_widget social_widget\">\r\n                    <div class=\"f_title\">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class=\"list\">\r\n                        <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-dribbble\"></i></a></li>\r\n                        <li><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>\r\n                    </ul>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(20, 'session_helpdesk', 'd98789iu8ghdjaw90d80po9', 0, 1, 1, '2018-07-23 00:00:00'),
(21, 'login_frontend_layout', '1', 0, 1, 1, '2019-03-27 00:00:00'),
(22, 'login_backend_layout', '1', 0, 1, 1, '2019-03-27 00:00:00'),
(23, 'login_layout', '4', 0, 0, 1, '2019-03-27 00:00:00'),
(24, 'api_key', '98ufu83476yrhdge', 0, 1, 1, '2019-03-27 00:00:00'),
(25, 'api_user', 'api_09283hdjks', 0, 1, 1, '2019-03-27 00:00:00'),
(26, 'api_pass', '098eq7312&_DSA', 0, 1, 1, '2019-03-27 00:00:00'),
(27, 'copyright', '2019 © ', 0, 1, 1, '2019-03-27 00:00:00'),
(28, 'login_developer_layout', '2', 0, 1, 1, '2019-03-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_configs`
--

CREATE TABLE `tbl_email_configs` (
  `id` int(32) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `port` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `mailtype` varchar(255) NOT NULL,
  `charset` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_configs`
--

INSERT INTO `tbl_email_configs` (`id`, `protocol`, `host`, `port`, `user`, `pass`, `mailtype`, `charset`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'smtp', 'smtp.gmail.com', '587', 'firman.begin@gmail.com', 'Ab1234abcd', 'html', 'iso-8859-1', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_layout`
--

CREATE TABLE `tbl_email_layout` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_layout`
--

INSERT INTO `tbl_email_layout` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'user_activation', ' <center>[date]</center><br/>                         Pengguna yang terhormat,                         <br/>                         <br/>                         Terima kasih telah melakukan registrasi akun di pesky indosporttiming, berikut detail data akun anda :                         email       : [email]<br/>                         username    : [username]<br/>                         password    : [password]<br/>                         status      : tidak aktif<br/>                         <br/>                             Untuk aktivasi account klik <b>[activation_link]</b><br/>                         Akun yang belum di aktifkan tidak akan bisa melakukan pendaftaran event lomba atau login kedalam dashboard indosporttiming<br/>                         Mohon untuk tidak men-sharing atau berbagi pakai dengan pihak lain terhadap akun dengan data diri anda agar tidak terjadi hal - hal yang tidak di inginkan.<br/>                     ', '-', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_links`
--

CREATE TABLE `tbl_email_links` (
  `id` int(32) NOT NULL,
  `email_layout_id` int(32) NOT NULL,
  `keyword` int(255) NOT NULL,
  `value` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_global_variables`
--

CREATE TABLE `tbl_global_variables` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_groups`
--

CREATE TABLE `tbl_groups` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_groups`
--

INSERT INTO `tbl_groups` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'superuser', '-', 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_permissions`
--

CREATE TABLE `tbl_group_permissions` (
  `id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `permission_id` int(32) NOT NULL,
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_group_permissions`
--

INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `is_allowed`, `is_public`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 0, 1, 1, '2019-05-07 19:54:15'),
(2, 1, 2, 1, 0, 1, 1, '2019-05-07 19:54:15'),
(3, 1, 3, 1, 0, 1, 1, '2019-05-07 19:54:15'),
(4, 1, 4, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(5, 1, 5, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(6, 1, 6, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(7, 1, 7, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(8, 1, 8, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(9, 1, 9, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(10, 1, 10, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(11, 1, 11, 1, 0, 1, 1, '2019-05-07 19:54:16'),
(12, 1, 12, 1, 0, 1, 1, '2019-05-07 19:55:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_icons`
--

CREATE TABLE `tbl_icons` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_icons`
--

INSERT INTO `tbl_icons` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'fa-500px', 'description', 1, 1, '2018-02-14 22:06:55'),
(2, 'fa-battery-1', '-', 1, 1, '2018-02-14 22:09:37'),
(3, 'fa-battery-empty', '-', 1, 1, '2018-02-14 22:10:42'),
(4, 'fa-battery-three-quarters', '-', 1, 1, '2018-02-14 22:13:07'),
(5, 'fa-calendar-plus-o', '-', 1, 1, '2018-02-14 22:13:35'),
(6, 'fa-chrome', '-', 1, 1, '2018-02-14 22:16:45'),
(7, 'fa-contao', '-', 1, 1, '2018-02-14 22:18:56'),
(8, 'fa-fonticons', '-', 1, 1, '2018-02-14 22:19:40'),
(9, 'fa-gg-circle', '-', 1, 1, '2018-02-14 22:20:30'),
(10, 'fa-hand-peace-o', '-', 1, 1, '2018-02-15 09:17:46'),
(11, 'fa-hand-spock-o', '-', 1, 1, '2018-02-15 09:18:34'),
(12, 'fa-hourglass-2', '-', 1, 1, '2018-02-15 09:19:37'),
(13, 'fa-hourglass-o', '-', 1, 1, '2018-02-17 00:31:49'),
(14, 'fa-industry', '-', 1, 1, '2018-02-18 20:15:10'),
(15, 'fa-map-pin', '-', 1, 1, '2018-02-18 20:17:28'),
(16, 'fa-object-ungroup', '-', 1, 1, '2018-02-18 20:19:00'),
(17, 'fa-opera', '-', 1, 1, '2018-02-18 20:19:53'),
(18, 'fa-sticky-note', '-', 1, 1, '2018-02-18 20:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_layouts`
--

CREATE TABLE `tbl_layouts` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_layouts`
--

INSERT INTO `tbl_layouts` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'login.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(2, 'login-2.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(3, 'login-3.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(4, 'login-4.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(5, 'login-5.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(6, 'login-5.min.css', '-', 1, 1, '2019-03-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menus`
--

CREATE TABLE `tbl_menus` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rank` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `module_id` int(32) DEFAULT NULL,
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menus`
--

INSERT INTO `tbl_menus` (`id`, `name`, `path`, `rank`, `level`, `icon`, `module_id`, `is_logged_in`, `is_active`, `description`, `parent_id`, `created_by`, `create_date`) VALUES
(1, 'Master', '#', 0, 1, '3', 2, 1, 1, '-', 0, 1, '2019-05-07 19:58:04'),
(2, 'Subjects', '#', 0, 2, '4', 2, 1, 1, '', 1, 1, '2019-05-07 19:58:28'),
(3, 'Categories', '#', 0, 2, '4', 2, 1, 1, '', 1, 1, '2019-05-07 19:58:44'),
(4, 'Types', '#', 0, 2, '3', 2, 1, 1, '', 1, 1, '2019-05-07 19:59:05'),
(5, 'Content', '#', 0, 1, '6', 2, 1, 1, '', 0, 1, '2019-05-07 20:01:29'),
(6, 'Create', '#', 0, 2, '12', 2, 1, 1, '', 5, 1, '2019-05-07 20:02:22'),
(7, 'View', '#', 0, 2, '4', 2, 1, 1, '', 5, 1, '2019-05-07 20:02:45'),
(8, 'Prefferences', '#', 0, 1, '7', 2, 1, 1, '', 0, 1, '2019-05-07 20:03:12'),
(9, 'User', '#', 0, 2, '5', 2, 1, 1, '', 8, 1, '2019-05-07 20:11:27'),
(10, 'Group', '#', 0, 2, '5', 2, 1, 1, '', 8, 1, '2019-05-07 20:11:42'),
(11, 'Group User', '#', 0, 2, '5', 2, 1, 1, '', 8, 1, '2019-05-07 20:11:59'),
(12, 'Permission', '#', 0, 2, '13', 2, 1, 1, '', 8, 1, '2019-05-07 20:12:20'),
(13, 'Group Permission', '#', 0, 2, '8', 2, 1, 1, '', 8, 1, '2019-05-07 20:12:35'),
(14, 'Menu', 'settings/menu/view', 0, 2, '7', 2, 1, 1, '', 8, 1, '2019-05-07 20:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_method_masters`
--

CREATE TABLE `tbl_method_masters` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rank` int(2) NOT NULL,
  `is_mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_method_masters`
--

INSERT INTO `tbl_method_masters` (`id`, `name`, `description`, `rank`, `is_mandatory`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'index', '-', 1, 1, 1, 1, '2018-10-17 00:00:00'),
(2, 'view', '-', 2, 1, 1, 2, '2018-10-17 00:00:00'),
(3, 'insert', '-', 3, 1, 1, 1, '2018-10-17 00:00:00'),
(4, 'remove', '-', 4, 1, 1, 2, '2018-10-17 00:00:00'),
(5, 'delete', '-', 5, 1, 1, 1, '2018-10-17 00:00:00'),
(6, 'dashboard', '-', 6, 0, 1, 1, '2018-10-17 21:39:30'),
(7, 'logout', '-', 7, 0, 1, 1, '2018-10-17 21:40:44'),
(8, 'login', '-', 8, 0, 1, 1, '2018-10-17 21:40:44'),
(9, 'auth', '-', 9, 0, 1, 1, '2018-10-17 21:40:02'),
(10, 'update', '-', 10, 1, 1, 1, '2018-10-17 00:00:00'),
(11, 'update_status', '-', 11, 0, 1, 1, '2018-10-17 00:00:00'),
(12, 'get_list', '-', 12, 1, 1, 1, '2018-10-17 00:00:00'),
(13, 'get_data', '-', 13, 1, 1, 1, '2018-10-17 00:00:00'),
(14, 'get_group', '-', 14, 0, 1, 1, '2018-10-17 00:00:00'),
(15, 'get_method', '-', 15, 0, 1, 1, '2018-10-17 00:00:00'),
(16, 'get_icon', '-', 16, 0, 1, 1, '2018-10-26 09:27:35'),
(17, 'get_menu', '-', 17, 0, 1, 1, '2018-10-29 19:57:39'),
(18, 'task', '-', 18, 0, 1, 1, '2018-11-20 21:27:47'),
(19, 'inbox', '-', 19, 0, 1, 1, '2018-11-20 21:27:58'),
(20, 'rank', '-', 20, 0, 1, 1, '2018-11-24 15:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'frontend', '-', 1, 1, '2019-01-18 00:00:00'),
(2, 'backend', '-', 1, 1, '2019-01-18 00:00:00'),
(3, 'vendor', '-', 1, 1, '2019-01-18 00:00:00'),
(4, 'api', '-', 1, 1, '2019-01-18 00:00:00'),
(5, 'developer', '-', 1, 1, '2019-01-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nb_applies`
--

CREATE TABLE `tbl_nb_applies` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nb_applies`
--

INSERT INTO `tbl_nb_applies` (`id`, `name`, `content`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'pengaplikasian lida buaya kepada rambut', '1. haluskan lida buaya\r\n2. beri sedikit air\r\n3. tambahkan es batu\r\n4. sedikit gula\r\n5. selesai', '-', 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nb_categories`
--

CREATE TABLE `tbl_nb_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rank` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL,
  `parent_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nb_categories`
--

INSERT INTO `tbl_nb_categories` (`id`, `name`, `path`, `rank`, `level`, `icon`, `parent_id`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Tumbuhan', '#', 1, 1, '-', 0, '1', 1, 1, '2019-05-04 00:00:00'),
(2, 'Hewan', '#', 1, 1, '-', 0, '-', 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nb_subjects`
--

CREATE TABLE `tbl_nb_subjects` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `benefit` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nb_subjects`
--

INSERT INTO `tbl_nb_subjects` (`id`, `title`, `benefit`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Lida Buaya', '- Menghilangkan ketombe\r\n- Menghilangkan ingatan', '-', 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nb_subject_trans`
--

CREATE TABLE `tbl_nb_subject_trans` (
  `id` int(32) NOT NULL,
  `subject_id` int(32) NOT NULL,
  `category_id` int(32) NOT NULL,
  `type_id` int(32) NOT NULL,
  `apply_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nb_subject_trans`
--

INSERT INTO `tbl_nb_subject_trans` (`id`, `subject_id`, `category_id`, `type_id`, `apply_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 5, 1, 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nb_types`
--

CREATE TABLE `tbl_nb_types` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nb_types`
--

INSERT INTO `tbl_nb_types` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'Tubuh', '-', 1, 1, '2019-05-04 00:00:00'),
(2, 'Wajah', '-', 1, 1, '2019-05-04 00:00:00'),
(3, 'Kuku', '-', 1, 1, '2019-05-04 00:00:00'),
(4, 'Mulut', '-', 1, 1, '2019-05-04 00:00:00'),
(5, 'Rambut', '-', 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permissions`
--

CREATE TABLE `tbl_permissions` (
  `id` int(32) NOT NULL,
  `module` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`id`, `module`, `class`, `action`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'backend', 'user', 'index', '', 1, 1, '2019-05-07 19:54:15'),
(2, 'backend', 'user', 'view', '', 1, 1, '2019-05-07 19:54:15'),
(3, 'backend', 'user', 'insert', '', 1, 1, '2019-05-07 19:54:15'),
(4, 'backend', 'user', 'remove', '', 1, 1, '2019-05-07 19:54:15'),
(5, 'backend', 'user', 'delete', '', 1, 1, '2019-05-07 19:54:16'),
(6, 'backend', 'user', 'update', '', 1, 1, '2019-05-07 19:54:16'),
(7, 'backend', 'user', 'get_list', '', 1, 1, '2019-05-07 19:54:16'),
(8, 'backend', 'user', 'get_data', '', 1, 1, '2019-05-07 19:54:16'),
(9, 'backend', 'user', 'dashboard', '', 1, 1, '2019-05-07 19:54:16'),
(10, 'backend', 'user', 'logout', '', 1, 1, '2019-05-07 19:54:16'),
(11, 'backend', 'user', 'auth', '', 1, 1, '2019-05-07 19:54:16'),
(12, 'backend', 'menu', '', '', 1, 1, '2019-05-07 19:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(32) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `activation_code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 => present, 2 => away, 3 => inactive',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `activation_code`, `status`, `is_active`, `is_logged_in`, `created_by`, `create_date`) VALUES
(1, 'arif', 'arif', 'firman syah', 'firman.begin@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 1, 1, '2019-01-16 00:00:00'),
(2, 'user', 'user', 'oreno', 'admin@admin.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 0, 1, '2019-01-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_groups`
--

CREATE TABLE `tbl_user_groups` (
  `id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_groups`
--

INSERT INTO `tbl_user_groups` (`id`, `user_id`, `group_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 1, '2019-05-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_profiles`
--

CREATE TABLE `tbl_user_profiles` (
  `id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(64) NOT NULL,
  `lng` varchar(64) NOT NULL,
  `zoom` int(4) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ajax_funcs`
--
ALTER TABLE `tbl_ajax_funcs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_categories`
--
ALTER TABLE `tbl_cms_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_category_contents`
--
ALTER TABLE `tbl_cms_category_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_comments`
--
ALTER TABLE `tbl_cms_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_contents`
--
ALTER TABLE `tbl_cms_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_content_photos`
--
ALTER TABLE `tbl_cms_content_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_messages`
--
ALTER TABLE `tbl_component_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_message_categories`
--
ALTER TABLE `tbl_component_message_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_message_labels`
--
ALTER TABLE `tbl_component_message_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notifications`
--
ALTER TABLE `tbl_component_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notification_categories`
--
ALTER TABLE `tbl_component_notification_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_notification_status`
--
ALTER TABLE `tbl_component_notification_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_task_categories`
--
ALTER TABLE `tbl_component_task_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_component_task_status`
--
ALTER TABLE `tbl_component_task_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_configs`
--
ALTER TABLE `tbl_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_configs`
--
ALTER TABLE `tbl_email_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_layout`
--
ALTER TABLE `tbl_email_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_groups`
--
ALTER TABLE `tbl_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_group_permissions`
--
ALTER TABLE `tbl_group_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_icons`
--
ALTER TABLE `tbl_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_layouts`
--
ALTER TABLE `tbl_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_method_masters`
--
ALTER TABLE `tbl_method_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nb_applies`
--
ALTER TABLE `tbl_nb_applies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nb_categories`
--
ALTER TABLE `tbl_nb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nb_subjects`
--
ALTER TABLE `tbl_nb_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nb_subject_trans`
--
ALTER TABLE `tbl_nb_subject_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_nb_types`
--
ALTER TABLE `tbl_nb_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_groups`
--
ALTER TABLE `tbl_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_profiles`
--
ALTER TABLE `tbl_user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ajax_funcs`
--
ALTER TABLE `tbl_ajax_funcs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cms_categories`
--
ALTER TABLE `tbl_cms_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_category_contents`
--
ALTER TABLE `tbl_cms_category_contents`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_comments`
--
ALTER TABLE `tbl_cms_comments`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_contents`
--
ALTER TABLE `tbl_cms_contents`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cms_content_photos`
--
ALTER TABLE `tbl_cms_content_photos`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_messages`
--
ALTER TABLE `tbl_component_messages`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_message_categories`
--
ALTER TABLE `tbl_component_message_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_message_labels`
--
ALTER TABLE `tbl_component_message_labels`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_notifications`
--
ALTER TABLE `tbl_component_notifications`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_notification_categories`
--
ALTER TABLE `tbl_component_notification_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_notification_status`
--
ALTER TABLE `tbl_component_notification_status`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_component_task_categories`
--
ALTER TABLE `tbl_component_task_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_component_task_status`
--
ALTER TABLE `tbl_component_task_status`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_configs`
--
ALTER TABLE `tbl_configs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_email_configs`
--
ALTER TABLE `tbl_email_configs`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_email_layout`
--
ALTER TABLE `tbl_email_layout`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_groups`
--
ALTER TABLE `tbl_groups`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_group_permissions`
--
ALTER TABLE `tbl_group_permissions`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_icons`
--
ALTER TABLE `tbl_icons`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_layouts`
--
ALTER TABLE `tbl_layouts`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_menus`
--
ALTER TABLE `tbl_menus`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_method_masters`
--
ALTER TABLE `tbl_method_masters`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_nb_applies`
--
ALTER TABLE `tbl_nb_applies`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nb_categories`
--
ALTER TABLE `tbl_nb_categories`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_nb_subjects`
--
ALTER TABLE `tbl_nb_subjects`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nb_subject_trans`
--
ALTER TABLE `tbl_nb_subject_trans`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nb_types`
--
ALTER TABLE `tbl_nb_types`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_user_groups`
--
ALTER TABLE `tbl_user_groups`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user_profiles`
--
ALTER TABLE `tbl_user_profiles`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
