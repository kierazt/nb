<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->get_all_history();
    }

    public function index() {
        redirect(base_url('home'));
    }
    public function home() {
        $data['logs'] = $this->get_all_history();
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $data['title_for_layout'] = 'welcome to dashbohard';
        $this->parser->parse('layouts/pages/metronic_full.phtml', $data);
    }   

}
