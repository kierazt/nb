<script>
    var fnLoadingImg = function (gif) {
        return '<img class="page-loading" src="' + static_url + 'images/' + gif + '"></img>';
    };

    var fnToStr = function (value, key, to) {
        if (dev_status) {
            switch (key) {
                case 'success':
                    toastr.success(value, key, {timeOut: to});
                    break;
                case 'warning':
                    toastr.warning(value, key, {timeOut: to});
                    break;
                case 'info':
                    toastr.info(value, key, {timeOut: to});
                    break;
                case 'error':
                    toastr.error(value, key, {timeOut: to});
                    break;
            }
        }
    };

    var fnCloseModal = function () {
        App.startPageLoading({animate: true});
        setTimeout(function () {
            $(".modal").hide();
            fnResetBtn();
            $("#add_edit")[0].reset();
            fnRefreshDataTable();
            App.stopPageLoading();
        }, 1100);
    };

    var fnResetBtn = function () {
        setInterval(function () {
            $.ajaxSetup({cache: false});
            App.startPageLoading();
            $("#opt_delete").attr("disabled", true);
            $("#opt_delete").addClass("disabled");

            $("#opt_remove").attr("disabled", true);
            $("#opt_remove").addClass("disabled");

            $("#opt_add").attr("disabled", false);
            $("#opt_add").removeClass("disabled");

            $("#opt_edit").attr("disabled", true);
            $("#opt_edit").addClass("disabled");
            App.stopPageLoading();
        }, 1100);
    };

    var fnCloseBootbox = function () {
        setInterval(function () {
            $.ajaxSetup({cache: false});
            App.startPageLoading();
            $(".bootbox").hide();
            $(".modal-backdrop").hide();
            fnRefreshDataTable();
            App.stopPageLoading();
        }, 1100);
    };

    var fnRefreshDataTable = function () {
        setInterval(function () {
            $.ajaxSetup({cache: false});
            App.startPageLoading();
            $(".table").DataTable().ajax.reload();
            App.stopPageLoading();
        }, 1100);
    };

    var GlobalAjax = function () {
        return {
            //main function to initiate the module
            init: function () {

                fnToStr('Global js ready!!!', 'success', 2000);

                $('button[type="button"]').on('click', function () {
                    var dismiss = $(this).attr('data-dismiss');
                    App.startPageLoading();
                    switch (dismiss) {
                        case 'modal':
                            setTimeout(function () {
                                $('.modal').modal('hide');
                                fnRefreshDataTable();
                                fnResetBtn();
                            }, 2100);
                            break;
                    }
                    App.stopPageLoading();
                });

                $('table#datatable_ajax').on('click', '#select_all', function () {
                    var is_checked = $(this).is(':checked');
                    if (is_checked == true) {
                        $('input[type="checkbox"]').prop('checked', true);
                    } else {
                        $('input[type="checkbox"]').prop('checked', false);
                    }
                });

                $('table#datatable_ajax').on('click', '.md-check', function () {
                    var id = $(this).attr('data-id');
                    var is_checked = $('input.select_tr').is(':checked');
                    if (is_checked == true) {
                        var count = $('input.select_tr').filter(':checked').length;
                        if (id) {
                            $('input[name="id"]').val(id);
                        }
                        if (count > 1)
                        {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", true);
                            $("#opt_edit").addClass("disabled");
                        } else {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", false);
                            $("#opt_edit").removeClass("disabled");
                        }
                    } else {
                        $("#opt_delete").attr("disabled", true);
                        $("#opt_delete").addClass("disabled");

                        $("#opt_remove").attr("disabled", true);
                        $("#opt_remove").addClass("disabled");

                        $("#opt_add").attr("disabled", false);
                        $("#opt_add").removeClass("disabled");

                        $("#opt_edit").attr("disabled", true);
                        $("#opt_edit").addClass("disabled");
                    }
                });
            }
        };
    }();

    jQuery(document).ready(function () {
        GlobalAjax.init();
    });
</script>
