<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mvc
 *
 * @author signet
 */
class Mvc extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_layout_controllers', 'Tbl_layout_models', 'Tbl_layout_views'));
    }

    public function generate() {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View MVC Generate tool';
        $data['content'] = 'ini kontent web';
        $data['modules'] = $this->get_module();
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/typeahead/typeahead.css')
        );
        $this->load_css($css_files);

        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/typeahead/handlebars.min.js'),
            static_url('templates/metronics/assets/global/plugins/typeahead/typeahead.bundle.min.js')
        );
        $this->load_js($js_files);

        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_data() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            switch (base64_decode($post['id'])) {
                case 1:
                    $res = $this->Tbl_layout_controllers->find('first', array('conditions' => array('is_active' => 1)));
                    break;

                case 2:
                    $res = $this->Tbl_layout_models->find('first', array('conditions' => array('is_active' => 1)));
                    break;

                case 3:
                    $res = $this->Tbl_layout_views->find('first', array('conditions' => array('is_active' => 1)));
                    break;
            }
            if (isset($res) && !empty($res)) {
                if (isset($res['script']) && !empty($res['script'])) {
                    $res['script'] = htmlspecialchars($res['script']);
                } else if (isset($res['view_html']) && !empty($res['view_html'])) {
                    $res['view_html'] = htmlspecialchars($res['view_html']);
                } else if (isset($res['view_js']) && !empty($res['view_js'])) {
                    $res['view_js'] = '&lt;script&gt;' . ($res['view_js']) . '&lt;/script&gt;';
                }
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function get_module() {
        $this->load->model(array('Tbl_modules'));
        $res = $this->Tbl_modules->find('list', array('conditions' => array('is_active' => 1), 'order' => array('key' => 'name', 'type' => 'ASC')));
        if (isset($res) && !empty($res)) {
            $arr = '';
            foreach ($res AS $k => $v) {
                $arr .= '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
            }
            return $arr;
        } else {
            return null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = '#collect parameter data... <br/>';
            $modules = $post['module'];
            $res .= '#module name [' . $modules . ']<br/>';
            $class_name_ucfirst = $post['class_name_ucfirst'];
            $res .= '#Class name [' . $class_name_ucfirst . ']<br/>';
            $class_base_url = $post['class_base_url'];
            $res .= '#using base_url => [' . $class_base_url . ']<br/>';
            $class_path = $post['class_path'];
            $model_name_ucfirst = isset($post['model_name_ucfirst']) ? $post['model_name_ucfirst'] : '-';
            //generate controller first
            //set app path
            $app_apth = $this->config->item('dir.app_modules', 'path');

            $res .= '#init app path [' . $app_apth . ']<br/>';
            //check controller path
            $class_path_full = $app_apth . DS . $modules . DS . 'controllers';
            $res .= '#define controller path [' . $class_path_full . ']<br/>';
            $extract_class_path = explode('/', $class_path);
            $last_class_path = count($extract_class_path) - 1;
            unset($extract_class_path[$last_class_path]);
            $opath_class = $class_path_full . DS . $extract_class_path[0];

            $res .= '#create directory for controller with path => [' . $opath_class . ']<br/>';
            if (!is_dir($opath_class)) {
                mkdir($opath_class, 0, true);
            }
            $sub_module = $extract_class_path[0];
            $this->load->helper('file');
            //get controller layout
            $controller = $this->Tbl_layout_controllers->find('first', array('conditions' => array('is_active' => 1)));
            $res .= '#fetch controller script layout...<br/>';
            $controller_data = $controller['script'];
            $controller_data = str_replace('[class_name_ucfirst]', $class_name_ucfirst, $controller_data);
            $controller_data = str_replace('[model_name_ucfirst]', $model_name_ucfirst, $controller_data);
            $controller_data = str_replace('[class_base_url]', $class_base_url, $controller_data);
            $controller_data = str_replace('[class_path]', implode('/', $extract_class_path) . '/', $controller_data);
            //write actual file
            $res .= '#define controller path and file name ' . $class_path_full . DS . $sub_module . DS . $class_name_ucfirst . '.php' . '<br/>';
            write_file($class_path_full . DS . $sub_module . DS . $class_name_ucfirst . '.php', $controller_data);
            //get model layout          
            if ($post['model_exist'] == 1) {
                if ($model_name_ucfirst != '-' || $model_name_ucfirst == '') {
                    $res .= '#Model name founded...<br/>';
                    $res .= '#fetch model script layout...<br/>';
                    $model = $this->Tbl_layout_models->find('first', array('conditions' => array('is_active' => 1)));
                    $model_data = $model['script'];
                    $model_data = str_replace('[model_name_ucfirst]', $model_name_ucfirst, $model_data);

                    $res .= '#set actual model into path and file name ' . $app_apth . DS . 'model' . DS . $model_name_ucfirst . '.php' . '<br/>';
                    write_file($app_apth . DS . 'model' . DS . $model_name_ucfirst . '.php', $model_data);
                }
            }
            $res .= '#finishing process, make view html and view js...<br/>';
            //check view html and js path            
            $view_path_full = $app_apth . DS . $modules . DS . 'views';
            $res .= '#defining view path ' . $view_path_full . '<br/>';
            $extract_view_path = $o_view_path = explode('/', $class_path);
            $last_view_path = count($extract_view_path) - 1;
            unset($extract_view_path[$last_view_path]);

            $opath_view = $view_path_full . DS . implode(DS, $extract_view_path);
            $fl_nm_view = $o_view_path[$last_view_path];
            if (!is_dir($opath_view)) {
                mkdir($opath_view, 0, true);
            }

            $res .= '#fetch views script layout...<br/>';
            //get view layout
            $view = $this->Tbl_layout_views->find('first', array('conditions' => array('is_active' => 1)));
            $view_html_data = $view['view_html'];
            $view_js_data = '<script>' . $view['view_js'];

            $view_js_data = str_replace('[class_name_ucfirst]', $class_name_ucfirst, $view_js_data);
            $view_js_data = str_replace('[model_name_ucfirst]', $model_name_ucfirst, $view_js_data);
            $view_js_data = str_replace('[class_base_url]', $class_base_url, $view_js_data);
            $view_js_data = str_replace('[class_path]', implode('/', $extract_class_path) . '/', $view_js_data);

            $res .= '#assign parameter value...<br/>';
            $view_html_data = str_replace('[model_name_ucfirst]', $model_name_ucfirst, $view_html_data);
            //write actual file
            $res .= '#set actual view html into path and file name ' . strtolower($view_path_full . DS . $sub_module . DS . $class_name_ucfirst . DS . $fl_nm_view) . '.html.php' . '<br/>';
            write_file(strtolower($view_path_full . DS . $sub_module . DS . $class_name_ucfirst . DS . $fl_nm_view) . '.html.php', $view_html_data);
            //write actual file
            $res .= '#set actual view js into path and file name ' . strtolower($view_path_full . DS . $sub_module . DS . $class_name_ucfirst . DS . $fl_nm_view) . '.js.php' . '<br/>';
            write_file(strtolower($view_path_full . DS . $sub_module . DS . $class_name_ucfirst . DS . $fl_nm_view) . '.js.php', $view_js_data . '</script>');
            $res .= 'generate complete!!!';
            echo $res;
        }
    }

}
