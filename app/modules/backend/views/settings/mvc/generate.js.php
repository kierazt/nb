<script>
    var numbers = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.num);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: [
            {num: 'base_url'},
            {num: 'base_backend_url'},
            {num: 'base_vendor_url'},
            {num: 'app_url'},
            {num: 'static_url'},
            {num: 'global_view_url'}
        ]
    });

    var fnGetTab = function (id) {
        var formdata = {
            id: Base64.encode(id)
        };
        $.ajax({
            url: base_backend_url + 'settings/mvc/get_data/',
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                var row = JSON.parse(response);
                if (row) {
                    if (id == 1) {
                        $('#controller_layout').html(row.script);
                    } else if (id == 2) {
                        $('#model_layout').html(row.script);
                    } else if (id == 3) {
                        $('#view_html_layout').html(row.view_html);
                        $('#view_js_layout').html(row.view_js);
                    }
                }
            },
            error: function () {
                fnToStr('Error is occured, please contact administrator.', 'error');
            }
        });
    };

    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                fnToStr('ajax js is ready', 'success');
                fnGetTab(1);
                $('a').on('click', function () {
                    var id = $(this).data('id');
                    if (id) {
                        var toggle = $(this).data('toggle');
                        if (toggle == 'tab') {
                            fnGetTab(id);
                        }
                    }
                });
                $('input[name="exist_model"]').on('click', function () {
                    var val = $(this).val();
                    $('#nmodel').fadeIn();
                });

                $('#class_base_url').typeahead(null, {
                    displayKey: 'num',
                    hint: (App.isRTL() ? false : true),
                    source: numbers.ttAdapter()
                });

                $("#gen_mvc").submit(function () {
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var uri = base_backend_url + 'settings/mvc/insert/';
                    var txt = 'add new class';
                    var model = $('input[name="model_name_ucfirst"]').val();
                    var rd = $('input[name="exist_model"]:checked').val();
                    var formdata = {
                        module: $('#module option:selected').text(),
                        class_name_ucfirst: $('input[name="class_name_ucfirst"]').val(),
                        class_base_url: $('input[name="class_base_url"]').val(),
                        class_path: $('input[name="class_path"]').val(),
                        model_name_ucfirst: model,
                        model_exist: rd,
                        active: is_active
                    };
                    console.log(formdata);
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            $('#result_generate').html(response);
                        },
                        error: function (response) {
                            toastr.error('Failed ' + txt);
                            $('#result_generate').html(response);
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>