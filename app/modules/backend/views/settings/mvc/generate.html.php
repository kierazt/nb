<style>
    pre{
        font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;
        margin-bottom: 10px;
        overflow: auto;
        width: auto;
        padding: 5px;
        background-color: #eee;
        padding-bottom: 20px;
        max-height: 600px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST" id="gen_mvc">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Module</label><br/>
                                <select class="form-control module" name="module" id="module">
                                    <option>-- select one --</option>
                                    <?php echo isset($modules) ? $modules : ''; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">class_name_ucfirst</label>
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                    <input class="form-control" type="text" name="class_name_ucfirst" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">class_base_url</label>
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                    <input class="form-control" type="text" id="class_base_url" name="class_base_url" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">class_path</label>
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                    <input class="form-control" type="text" name="class_path" /> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Radio</label>
                                <div class="col-md-9">
                                    <div class="radio-list">
                                        <label>
                                            <div class="radio">
                                                <span>
                                                    <input type="radio" name="exist_model" id="exist_model" value="1">
                                                </span>
                                            </div> Create New Model 
                                        </label>
                                        <label>
                                            <div class="radio" >
                                                <span class="checked">
                                                    <input type="radio" name="exist_model" id="exist_model" value="2">
                                                </span>
                                            </div> Select exist Model
                                        </label>    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="nmodel" hidden>
                                <label class="control-label">model_name_ucfirst</label>
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                    <input class="form-control" type="text" name="model_name_ucfirst" /> 
                                </div>
                            </div>
                            <div class="form-group" style="height:30px">
                                <label>Active</label><br/>
                                <input type="checkbox" class="make-switch" data-size="small" name="status"/>
                            </div>
                        </div>
                        <div class="col-md-3"><pre><code><div id="result_generate"></div></code></pre></div>
                        <div class="col-md-6">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-id="1" data-toggle="tab"> Controller </a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-id="2" data-toggle="tab"> Model </a>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> View
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="#tab_1_3" data-id="3" tabindex="-1" data-toggle="tab"> HTML </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_4" data-id="3" tabindex="-1" data-toggle="tab"> JS </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_1_1">
                                    <pre><code id="controller_layout"></code></pre>
                                </div>
                                <div class="tab-pane fade" id="tab_1_2">
                                     <pre><code id="model_layout"></code></pre>
                                </div>
                                <div class="tab-pane fade" id="tab_1_3">
                                     <pre><code id="view_html_layout"></code></pre>
                                </div>
                                <div class="tab-pane fade" id="tab_1_4">
                                    <pre><code id="view_js_layout"></code></pre>
                                </div>
                            </div>
                            <div class="clearfix margin-bottom-20"> </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <button type="submit" class="btn green">Execute</button>
                </form>
            </div>
        </div>
    </div>
</div>