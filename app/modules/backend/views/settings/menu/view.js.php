<script>
    var fnInitTree = function (id, logged, name) {
        $.ajax({
            async: true,
            type: "GET",
            url: base_backend_url + ('settings/menu/get_menu/' + id + '/' + logged + '/' + name),
            dataType: "json",
            success: function (json) {
                var el = '#tree_' + id;
                $(el).treeview({
                    data: json,
                    showCheckbox: true,
                    onNodeSelected: function (event, data) {
                        $(el).treeview('uncheckAll', {silent: true});
                        $('#opt_add').fadeOut();

                        $('#opt_edit').fadeIn();
                        $('#opt_edit').removeAttr('disabled');
                        $('#opt_edit').removeClass('disabled');

                        $('#opt_delete').fadeIn();
                        $('#opt_delete').removeAttr('disabled');
                        $('#opt_delete').removeClass('disabled');

                        $('#opt_remove').fadeIn();
                        $('#opt_remove').removeAttr('disabled');
                        $('#opt_remove').removeClass('disabled');

                        $('#opt_edit').attr('data-id', data.id);
//                        $('#opt_edit').attr('data-module_id', data.module_id);
//                        $('#opt_edit').attr('data-module_name', data.module_name);
//                        $('#opt_edit').attr('data-parent_id', data.parent_id);
//                        $('#opt_edit').attr('data-parent_name', data.parent_name);
//                        $('#opt_edit').attr('data-level', data.level);
//                        $('#opt_edit').attr('data-text', data.text);
//                        $('#opt_edit').attr('data-micon', data.micon);
//                        $('#opt_edit').attr('data-href', data.href);
//                        $('#opt_edit').attr('data-is_active', data.is_active);
//                        $('#opt_edit').attr('data-is_logged_in', data.is_logged_in);
//                        $('#opt_edit').attr('data-description', data.description);

                        $('#opt_delete').attr('data-id', data.id);

                        $('.actions').show();
                    },
                    onNodeChecked: function (event, data) {
                        $('#add_edit')[0].reset();
                        $(el).treeview('unselectNode', [data.nodeId, {silent: true}]);
                        $('#opt_add').fadeIn();
                        $('#opt_edit').fadeOut();
                        $('#opt_delete').fadeOut();
                        $('#opt_add').attr('data-module_id', data.module_id);
                        $('#opt_add').attr('data-module_name', data.module_name);
                        $('#opt_add').attr('data-parent_id', data.id);
                        $('#opt_add').attr('data-parent_name', data.text);
                        $('#opt_add').attr('data-level', data.level);
                        $('.actions').show();
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    };

    var Index = function () {
        return {
            //main function to initiate the module
            init: function () {
                fnToStr('menu js is ready', 'success');

                var module_id_exist = $('li.active a').attr('data-module_id');
                var module_name_exist = $('li.active a').attr('data-module_name');

                fnInitTree(module_id_exist, 1, module_name_exist);

                $('a').on('click', function () {
                    if ($(this).attr('data-type') == 'tab') {
                        var href = $(this).attr('href');
                        var module_id = $(this).attr('data-module_id');
                        var module_name = $(this).attr('data-module_name');
                        fnInitTree(module_id, 1, module_name);
                    }
                });

                $('.btn').on('click', function () {
                    var cls = $(this).attr('id');
                    var action = $(this).attr('data-action');
                    if (action == 'close-modal') {
                        $('#add_edit')[0].reset();
                    }
                    switch (cls) {
                        case 'opt_refresh':
                            break;

                        case 'opt_add':
                            $('#title_mdl').html('Add new menu');
                            $('input[name="module_id"]').val($(this).attr('data-module_id'));
                            $('input[name="module_name"]').val($(this).attr('data-module_name'));
                            $('input[name="parent_id"]').val($(this).attr('data-parent_id'));
                            $('input[name="parent_name"]').val($(this).attr('data-parent_name'));
                            $('input[name="level"]').val($(this).attr('data-level'));

                            $('#modal_add_edit').show();
                            break;

                        case 'opt_edit':
                            $('#title_mdl').html('Update exist menu');
                            var id = $(this).attr('data-id');
                            var formdata = {
                                id: Base64.encode(id)
                            };
                            var uri = base_backend_url + 'settings/menu/get_data/';
                            $.ajax({
                                url: uri,
                                method: "POST", //First change type to method here
                                data: formdata,
                                success: function (response) {
                                    var row = JSON.parse(response);
                                    console.log(row);
                                    if (row) {
                                        var status_ = false;
                                        if (row.is_active == 1) {
                                            status_ = true;
                                        }
                                        var logged_ = false;
                                        if (row.is_logged_in == 1) {
                                            logged_ = true;
                                        }

                                        $('input[name="id"]').val(row.id);
                                        $('input[name="module_id"]').val(row.module_id);
                                        $('input[name="module_name"]').val(row.module_name);
                                        $('input[name="parent_id"]').val(row.parent_id);
                                        $('input[name="parent_name"]').val(row.parent_name);
                                        $('input[name="level"]').val(row.level);
                                        $('input[name="name"]').val(row.name);
                                        $('input[name="path"]').val(row.path);
                                        $("#icon select").val(row.icon_id);
                                        $("[name='status']").bootstrapSwitch('state', status_);
                                        $("[name='logged']").bootstrapSwitch('state', logged_);
                                        $('textarea[name="description"]').val(row.description);

                                        $('#modal_add_edit').show();
                                    }
                                },
                                error: function () {
                                    toastr.error('Failed ' + response);
                                    $('#tree_' + module).treeview('remove');
                                }
                            });
                            break;
                    }
                });

                $('#submit_add_edit').on('click', function () {
                    var id = $('input[name="id"]').val();
                    var parent_id = $('input[name="parent_id"]').val();
                    var level = $('input[name="level"]').val();
                    var path = $('input[name="path"]').val();
                    var module = $('input[name="module_id"]').val();
                    var is_active = $('input[name="status"]').bootstrapSwitch('state');
                    var is_logged_in = $('input[name="logged"]').bootstrapSwitch('state');
                    var formdata = {
                        name: $('input[name="name"]').val(),
                        parent_id: parent_id,
                        level: level,
                        icon: $('#icon').val(),
                        path: path,
                        module: module,
                        description: $('textarea[name="description"]').val(),
                        active: is_active,
                        logged: is_logged_in
                    };

                    var uri = base_backend_url + 'settings/menu/insert/';
                    if (id) {
                        uri = base_backend_url + 'settings/menu/update/';
                        var formdata = {
                            id: Base64.encode(id),
                            name: $('input[name="name"]').val(),
                            parent_id: parent_id,
                            level: level,
                            icon: $('#icon').val(),
                            path: path,
                            module: module,
                            description: $('textarea[name="description"]').val(),
                            active: is_active,
                            logged: is_logged_in
                        };
                    }

                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('success ' + response);
                            $('#tree_' + module).treeview('remove');
                            fnInitTree(module, 1, module_name_exist);
                            fnCloseModal();
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            $('#tree_' + module).treeview('remove');
                            fnInitTree(module, 1, module_name_exist);
                            fnCloseModal();
                        }
                    });
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Index.init();
    });
</script>