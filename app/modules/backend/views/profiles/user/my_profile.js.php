<script>
    var fnAssignForm = function(){
      $.post(base_backend_url + 'profiles/user/get_user_profile/', function (data) {
          var row = JSON.parse(data);
          var status_ = false;
          if (row.content.is_active == 1) {
              status_ = true;
          }
          $('input[name="id"]').val(row.content.id);
          $('input[name="name"]').val(row.content.username);
          $("[name='status']").bootstrapSwitch('state', status_);
          $('textarea[name="address"]').val(row.description);
      });
    };

    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('profile js ready!!!');
                fnAssignForm();
                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_backend_url + 'app/category/get_data/' + id, function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            $('input[name="id"]').val(row.id);
                            $('input[name="name"]').val(row.name);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $('textarea[name="description"]').val(row.description);
                        });
                    }
                });


                $("#add_edit").submit(function () {
                    var id = $('input[name="id"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var uri = base_backend_url + 'app/category/insert/';
                    var txt = 'add new group';
                    var formdata = {
                        name: $('input[name="name"]').val(),
                        description: $('textarea[name="description"]').val(),
                        active: is_active
                    };
                    if (id)
                    {
                        uri = base_backend_url + 'app/category/update/';
                        txt = 'update group';
                        formdata = {
                            id: Base64.encode(id),
                            name: $('input[name="name"]').val(),
                            description: $('textarea[name="description"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            fnCloseModal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            fnCloseModal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>
