<script>
    var TableDatatablesAjax = function () {

        var fnGetSetContent = function (id) {
            $.ajax({
                url: base_url + 'backend/app/content/get_data/' + id,
                method: "POST", //First change type to method here
                success: function (response) {
                    //$('#summernote').html(response);
                    $('#summernote').summernote('insertText', response);
                    return false;
                },
                error: function () {
                    return false;
                }

            });
        }

        return {
            //main function to initiate the module
            init: function () {
                $('.get_menu').on('click', function () {
                    $.ajax({
                        url: base_url + 'backend/app/content/get_menu/',
                        method: "POST", //First change type to method here
                        success: function (response) {
                            $('.set_menu').html(response);
                        },
                        error: function (response) {
                            toastr.error('Failed ' + response);
                        }

                    });
                });

                $('#summernote').summernote({
                    placeholder: 'text'
                });

                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'backend/app/content/get_list',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "title"},
                        {"data": "menu"},
                        {"data": "description"},
                        {"data": "active"},
                        {"data": "content_status"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (settings) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    console.log(state); // true | false
                    var id = $(this).attr('data-id');
                    var formdata = {
                        active: state
                    };
                    $.ajax({
                        url: base_url + 'backend/app/content/update_status/' + Base64.encode(id),
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + response);
                            return false;
                        },
                        error: function (response) {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });

                $('#opt_add_e').on('click', function () {
                    App.startPageLoading({animate: true});
                    setTimeout(function () {
                        App.stopPageLoading();
                        window.location = base_url + 'backend/app/content/add_content';
                    }, 2000);
                });

                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_url + 'backend/app/content/get_data/' + id, function (data) {
                            //console.log(data);
                            var row = JSON.parse(data);
                            //console.log(row);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            $('input[name="id"]').val(row.id);
                            $('input[name="title"]').val(row.title);
                            //fnGetSetContent(row.content);	
                            //$('#summernote').summernote('code', row.text);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $('textarea[name="description"]').val(row.description);
                        });
                    }
                });

                $("#opt_delete").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_url + 'settings/content/delete/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        close_bootbox();
                                        close_modal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        close_bootbox();
                                        close_modal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling delete data ');
                                close_bootbox();
                                close_modal();
                                return false;
                            }
                        });

                    } else {
                        toastr.success('Something went wrong ');
                        close_bootbox();
                        close_modal();
                        return false;
                    }
                });

                $("#opt_remove").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_url + 'settings/content/delete/' + Base64.encode(id),
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        close_bootbox();
                                        close_modal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        close_bootbox();
                                        close_modal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling remove data ');
                                close_bootbox();
                                close_modal();
                                return false;
                            }
                        });
                    } else {
                        toastr.success('Something went wrong ');
                        close_bootbox();
                        close_modal();
                        return false;
                    }
                });
                $("#add_edit").submit(function () {
                    var id = $('input[name="id"]').val();
                    var title = $('input[name="title"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var menu = $('#mmenu').val();
                    var content = $('#summernote').summernote('code');
                    var uri = base_url + 'backend/app/content/insert/';
                    var txt = 'add new content';
                    var formdata = {
                        title: title,
                        content: content,
                        menu: menu,
                        description: $('textarea[name="description"]').val(),
                        active: is_active
                    };
                    //console.log(formdata);
                    //return false;
                    if (id)
                    {
                        uri = base_url + 'backend/app/content/update/';
                        txt = 'update group';
                        formdata = {
                            id: Base64.encode(id),
                            title: title,
                            content: content,
                            menu: menu,
                            description: $('textarea[name="description"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            close_modal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            close_modal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>