<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//gobal router
$route['default_controller'] = 'frontend/user';
$route['/'] = 'frontend/user/index';
$route['home'] = 'frontend/user/home';

//backend route
$route['backend'] = 'backend/settings/user/login';
$route['backend/auth-user'] = 'backend/settings/user/auth';
$route['backend/login'] = 'backend/settings/user/login';
$route['backend/logout'] = 'backend/settings/user/logout';
$route['backend/dashboard'] = 'backend/settings/user/dashboard';
$route['backend/my-profile'] = 'backend/profiles/user/my_profile';
$route['backend/my-inbox'] = 'backend/profiles/user/my_inbox';
$route['backend/my-task'] = 'backend/profiles/user/my_task';
$route['backend/lock-screen'] = 'backend/profiles/user/lock_screen';
$route['backend/unlock-screen'] = 'backend/profiles/user/un_lock_screen';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
