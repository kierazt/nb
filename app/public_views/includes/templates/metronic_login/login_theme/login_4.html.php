<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?php echo base_url(); ?>">
        <!--<img style="width:10%" src="<?php// echo static_url('images\logo') ?>" alt="" />--> 
        <br/>
        <h3 style="color:#fff">Nature Beauty</h3>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">   
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="post">
        <h3 class="form-title"><?php echo $this->lang->line('global_log_in_to_account'); ?></h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> <?php echo $this->lang->line('global_enter_username_password'); ?> </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"> <?php echo $this->lang->line('global_username'); ?> </label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php echo $this->lang->line('global_username'); ?>" name="username" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('global_password'); ?></label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo $this->lang->line('global_password'); ?>" name="password" /> </div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input type="checkbox" name="remember" value="1" /> <?php echo $this->lang->line('global_remembered_me'); ?> </label>
            <button type="submit" class="btn green pull-right"> <?php echo $this->lang->line('global_login'); ?> </button>
        </div>
        <div class="forget-password">
            <h4><?php echo $this->lang->line('global_forgot_password'); ?></h4>
            <p> <?php echo $this->lang->line('global_click'); ?> <a href="javascript:;" id="forget-password"> <?php echo $this->lang->line('global_here'); ?> </a> <?php echo $this->lang->line('global_reset_pass'); ?>. </p>
        </div>

    </form>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright"> <?php echo isset($_ajax_var_configs->copyright) ? $_ajax_var_configs->copyright : '';?> </div>