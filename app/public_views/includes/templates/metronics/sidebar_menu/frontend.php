<?php $path = ''; ?> 
<?php if ($_var_template->_module == 'frontend'): ?>
    <?php if (isset($_menu) && !empty($_menu)): ?>
        <li class="heading">
            <h3 class="uppercase"><?php echo $_menu['text']; ?></h3>
        </li>
        <?php if (isset($_menu['nodes']) && !empty($_menu['nodes'])): ?>
            <?php foreach ($_menu['nodes'] AS $key => $values): ?>
                <li class="nav-item ">
                    <a href="<?php echo $path = ($values['menu_path'] == '#' || $values['menu_path'] == '') ? 'javascript:;' : base_url($values['menu_path']); ?>" class="nav-link <?php echo $path = ($values['menu_path'] == '#' || $values['menu_path'] == '') ? 'nav-toggle' : ''; ?>">
                        <i class="fa <?php echo $values['menu_icon']; ?>"></i>
                        <span class="title"><?php echo $values['menu_text']; ?></span>
                        <?php echo $path = ($values['menu_path'] == '#' || $values['menu_path'] == '') ? '<span class="arrow"></span>' : ''; ?>
                    </a>
                    <ul class="sub-menu">
                        <?php if (isset($values['nodes']) && !empty($values['nodes'])): ?>
                            <?php foreach ($values['nodes'] AS $k => $val): ?>
                                <li class="nav-item  ">
                                    <a href="<?php echo $path = ($val['menu_path'] == '#' || $val['menu_path'] == '') ? 'javascript:;' : base_url($val['menu_path']); ?>" class="nav-link <?php echo $path = ($val['menu_path'] == '#' || $val['menu_path'] == '') ? 'nav-toggle' : ''; ?>">
                                        <i class="fa <?php echo $val['menu_icon']; ?>"></i>
                                        <span class="title"><?php echo $val['menu_text']; ?></span>                                        
                                        <?php echo $path = ($val['menu_path'] == '#' || $val['menu_path'] == '') ? '<span class="arrow"></span>' : ''; ?>
                                    </a>
                                    <ul class="sub-menu">
                                        <?php if (isset($val['nodes']) && !empty($val['nodes'])): ?>
                                            <?php foreach ($val['nodes'] AS $k => $v): ?>
                                                <li class="nav-item  ">
                                                    <a href="<?php echo $path = ($v['menu_path'] == '#' || $v['menu_path'] == '') ? 'javascript:;' : base_url($v['menu_path']); ?>" class="nav-link <?php echo $path = ($v['menu_path'] == '#' || $v['menu_path'] == '') ? 'nav-toggle' : ''; ?>">
                                                        <i class="fa<?php echo $v['menu_icon']; ?>"></i>
                                                        <span class="title"><?php echo $v['menu_text']; ?></span>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endforeach; ?>		
        <?php endif; ?>
    <?php endif; ?>	
<?php endif; ?>