<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <p style="float:left; margin: 5px 0; width:80%">
                <a href="<?php echo base_url($_var_template->_base_url . 'dashboard'); ?>">
                    <img style="width:20%" src="<?php echo static_url('images\logo') ?>" alt="logo" /> 
                    <small>Nature Beauty</small>
                </a>
            </p>
            <div style="float:right; margin: 5px 0; width:20%" class="menu-toggler sidebar-toggler"> </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="<?php echo isset($_load_auth_config_var['img']) ? static_url($_load_auth_config_var['img']) : static_url('templates/metronics/assets/layouts/layout/img/avatar3_small.jpg'); ?>" />
						<span class="username username-hide-on-mobile"> <?php echo isset($_load_auth_config_var['email']) ? $_load_auth_config_var['email'] : ''; ?> </span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url($_var_template->_base_url . 'lock-screen'); ?>">
						<i class="icon-lock"></i> Lock Screen </a>
				</li>
				<li>
					<a href="<?php echo base_url($_var_template->_base_url . 'logout'); ?>">
						<i class="icon-logout"></i> Log Out </a>
				</li>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
